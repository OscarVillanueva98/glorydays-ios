//
//  MemoryCell.swift
//  GloryDays
//
//  Created by Oscar Javier Villanueva Prieto on 12/06/18.
//  Copyright © 2018 Oscar Javier Villanueva Prieto. All rights reserved.
//

import UIKit

class MemoryCell: UICollectionViewCell {
    @IBOutlet var imageview: UIImageView!
}
