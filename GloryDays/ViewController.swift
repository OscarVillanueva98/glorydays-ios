//
//  ViewController.swift
//  GloryDays
//
//  Created by Oscar Javier Villanueva Prieto on 12/06/18.
//  Copyright © 2018 Oscar Javier Villanueva Prieto. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Speech

class ViewController: UIViewController {
    @IBOutlet var labelInfo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func askForPermissions(_ sender: Any) {
        self.askForPhotosPermission()
    }
    
    //Pedimos permisos para hacer uso de las fotos
    func askForPhotosPermission(){
        PHPhotoLibrary.requestAuthorization { [unowned self](authStatus) in
            //Traer todo desde el hilo secundario al hilo principal
            DispatchQueue.main.async {
                if authStatus == .authorized {
                    self.askForRecordPermission()
                }
                else{
                    self.labelInfo.text = "Has dicho que no a los permisos de fotos para poder continuar es necesario activarlos desde ajustes"
                }
            }
        }
    }
    
    //Pedimos permisos para grabar
    func askForRecordPermission(){
        AVAudioSession.sharedInstance().requestRecordPermission { [unowned self](allow) in
            DispatchQueue.main.async {
                if allow {
                    self.askForTranscriptionPermission()
                }
                else{
                    self.labelInfo.text = "Has dicho que no a los permisos de grabación de audio para poder continuar es necesario activarlos desde ajustes"
                }
            }
        }
    }
    
    //Pedimos permisos para transcribir
    func askForTranscriptionPermission(){
        SFSpeechRecognizer.requestAuthorization { [unowned self](authStatus) in
            DispatchQueue.main.async {
                if authStatus == .authorized {
                    self.authorizationCompleted()
                }
                else{
                    self.labelInfo.text = "Has dicho que no a los permisos de transcripcion de texto para poder continuar es necesario activarlos desde ajustes"
                }
            }
        }
    }
    
    func authorizationCompleted(){
        dismiss(animated: true)
    }
}

