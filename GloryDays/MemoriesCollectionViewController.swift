//
//  MemoriesCollectionViewController.swift
//  GloryDays
//
//  Created by Oscar Javier Villanueva Prieto on 12/06/18.
//  Copyright © 2018 Oscar Javier Villanueva Prieto. All rights reserved.
//

import UIKit

import Photos
import AVFoundation
import Speech

import CoreSpotlight
import MobileCoreServices

private let reuseIdentifier = "cell"

class MemoriesCollectionViewController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout, AVAudioRecorderDelegate, UISearchBarDelegate {

    var memories : [URL] = []
    var filteredMemories : [URL] = []
    
    var currentMemory : URL!
    
    var audioRecorder : AVAudioRecorder?
    var audioPlayer : AVAudioPlayer?
    var recordingURL : URL!
    
    var searchQuery : CSSearchQuery?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.recordingURL = documentDirectory().appendingPathComponent("memory-recording.m4a")
        
        self.loadMemories()
        
        self.reloadSearch()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addImagePress))

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        //self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkForGrantedPermissions()
    }
    
    
    //Eliminamos el error que había que no mostraba las imagenes
    func reloadSearch(){
        if filteredMemories.count < 1 {
            return
        }
        
        CSSearchableIndex.default().deleteSearchableItems(withIdentifiers: ["com.practica.GloryDays"], completionHandler: nil)
        
        //print(filteredMemories.count)
        
        for files in filteredMemories {
            let transciption = self.transcriptionURL(form: files).path
            if let text = try? String(contentsOfFile: transciption){
                indexMemory(memory: files, text: text)
            }
        }
    }
    
    //MARK: - Check the permissions
    func checkForGrantedPermissions(){
        let photosAuth : Bool = PHPhotoLibrary.authorizationStatus() == .authorized
        let recordingAuth : Bool = AVAudioSession.sharedInstance().recordPermission() == .granted
        let trascriptionAuth : Bool = SFSpeechRecognizer.authorizationStatus() == .authorized
        let authorized = photosAuth && recordingAuth && trascriptionAuth
        if !authorized {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "showTerms"){
                navigationController?.present(vc, animated: true)
            }
        }
    }
    
    //MARK: - Load the photos
    func loadMemories(){
        //Vaceamos el arreglo para evitar duplicados
        self.memories.removeAll()
        
        //Obtenemos los archivos
        guard let files = try? FileManager.default.contentsOfDirectory(at: documentDirectory(), includingPropertiesForKeys: nil, options: []) else{
            return
        }
        
        //Recorremos los arhivos para sacar su nombre, lastPathComponent guarda el nombre
        for file in files {
            let fileName = file.lastPathComponent
            
            if fileName.hasSuffix(".thumb"){
                //Le quitamos la extension
                let noExtension = fileName.replacingOccurrences(of: ".thumb", with: "")
                //Obtenemos la ruta de los archivos ya sea imagen, audio o el thumb con solo agregarle la extension
                let memoryPath = documentDirectory().appendingPathComponent(noExtension)
                memories.append(memoryPath)
            }
        }
        
        filteredMemories = memories
        //Recargamos la seccion 1 la 0 es la caja de busqueda
        collectionView?.reloadSections(IndexSet(integer: 1))
    }
    
    //Document Directory sacamos la ruta donde vamos a guardar todos los archivos
    func documentDirectory() -> URL{
        //.documentDirectory es donde se guardan todos los archivos de las apps
        //txt,img y todo
        //.userDomainMask donde se guardan los datos generados por el usuario todos imagenes audios etc
        //Saca la carpeta documentos reservada para esta app
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        //tomamos la primera sección
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    @objc func addImagePress(){
        let vc = UIImagePickerController()
        vc.modalPresentationStyle = .formSheet
        vc.delegate = self
        navigationController?.present(vc, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let theImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.addNewMemory(image: theImage)
            self.loadMemories()
            
            dismiss(animated: true)
        }
    }
    
    func addNewMemory(image: UIImage){
        //Identificamos cada fecha
        let memoryName = "memory-\(Date().timeIntervalSince1970)"
        let imageName = "\(memoryName).jpg"
        let thumbName = "\(memoryName).thumb"
        
        do{
            let imagePath = documentDirectory().appendingPathComponent(imageName)
            //Mapeamos la imagen
            if let jpegData = UIImageJPEGRepresentation(image, 50){
                try jpegData.write(to: imagePath,options: [.atomicWrite])
            }
            
            if let thumbail = resizeImage(image: image, to: 150){
                let thumbPath = documentDirectory().appendingPathComponent(thumbName)
                
                if let jpegData = UIImageJPEGRepresentation(thumbail, 50) {
                    try jpegData.write(to: thumbPath, options: [.atomicWrite])
                }
            }
        }
        catch{
            print("Ha falldo la escritura en disco")
        }
    }
    
    func resizeImage(image : UIImage, to width: CGFloat) -> UIImage?{
        //porcentaje por el cual vamos a reducir
        let scaleFactor = width/image.size.width
        let height = image.size.height * scaleFactor
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 0)
        
        image.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    //Sacamos la imagen
    func imageURL(form memory: URL) -> URL{
        return memory.appendingPathExtension("jpg")
    }
    
    //Sacamos la miniatura
    func thumbailURL(form memory: URL) -> URL{
        return memory.appendingPathExtension("thumb")
    }
    
    //Sacamos el audio
    func audioURL(form memory: URL) -> URL{
        return memory.appendingPathExtension("m4a")
    }
    
    //Sacamos la transcripcion
    func transcriptionURL(form memory: URL) -> URL{
        return  memory.appendingPathExtension("txt")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        //Este if para el buscador
        if section == 0{
            return 0
        }
        else{
            //return self.memories.count
            return self.filteredMemories.count
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MemoryCell
        
        // Configure the cell
        let memory = self.filteredMemories[indexPath.row]
        let memoryName = self.thumbailURL(form: memory).path
        let image = UIImage(contentsOfFile: memoryName)
        cell.imageview.image = image
        
        //Agregamos una pulsacion larga
        if cell.gestureRecognizers == nil {
            let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.memoryLongPress(sender:)))
            recognizer.minimumPressDuration = 0.3
            cell.addGestureRecognizer(recognizer)
            
            //Le agregamos un bordecito
            //layer es como style
            cell.layer.borderColor = UIColor.white.cgColor
            cell.layer.borderWidth = 4
            cell.layer.cornerRadius = 10
        }
        
        return cell
    }
    
    @objc func memoryLongPress(sender: UILongPressGestureRecognizer){
        if sender.state == .began {
            let cell = sender.view as! MemoryCell
            if let index = collectionView?.indexPath(for: cell) {
                self.currentMemory = self.filteredMemories[index.row]
                self.startRecordingMemory()
            }
        }
        else{
            if sender.state == .ended {
                self.finishRecordingMemory(success: true)
            }
        }
    }
    
    func startRecordingMemory(){
        //Paramos las grabaciones
        audioPlayer?.stop()
        
        collectionView?.backgroundColor = UIColor(red: 0.6, green: 0, blue: 0, alpha: 1)
        
        //sesión de grabación
        let recordingSession = AVAudioSession.sharedInstance()
        do{
            //Definimos que vamos a hacer con el audio en este caso lo queeremos para grabar y despues
            //reproducir y tomamos las configuraciones básicas del microfono
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
            
            //Iniciamos la grabación
            try recordingSession.setActive(true)
            
            //Ajustes
            let recordingSettings = [
                AVFormatIDKey : Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey : 44100,
                AVNumberOfChannelsKey : 2,
                AVEncoderAudioQualityKey : AVAudioQuality.high.rawValue
            ]
            
            audioRecorder = try AVAudioRecorder(url: recordingURL, settings: recordingSettings)
            audioRecorder?.delegate = self
            
            audioRecorder?.record()
        }
        catch let error{
            print(error.localizedDescription)
            finishRecordingMemory(success: false)
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        //Solo se capturamos cuando no hubo exito al grabar
        if !flag {
            finishRecordingMemory(success: flag)
        }
    }
    
    func finishRecordingMemory(success: Bool){
        collectionView?.backgroundColor = UIColor(red: 158/255, green: 184/255, blue: 1, alpha: 1)
        audioRecorder?.stop()
        
        if success {
            do{
                //Currentmemorie ya trae la URL donde vamos guardar
                //por eso solo le agregamos la extensión
                let memorieAudioURL = self.currentMemory.appendingPathExtension("m4a")
                
                //Iniciamos el fileManager
                let fileManager = FileManager.default
                
                //verificamos si existe el audio
                if fileManager.fileExists(atPath: memorieAudioURL.path) {
                
                    //si existe lo borramos
                    try fileManager.removeItem(at: memorieAudioURL)
                }
                
                //movemos el nuevo audio de donde se quedo de la grabación o donde debe ir correctamente
                try fileManager.moveItem(at: recordingURL, to: memorieAudioURL)
                
                //Transcribimos el audio
                self.transcribeAudioToText(memory: self.currentMemory)
                    
                
                
            }
            catch let error{
                print("Ha habido un error\(error.localizedDescription)")
            }
        }
    }
    
    func transcribeAudioToText(memory: URL){
        //Sacamos el audio
        let audio = audioURL(form: memory)
        
        //Obtenemos la transcripcion
        let transcription = transcriptionURL(form: memory)
        
        //Creamos el traductor
        let recognizer = SFSpeechRecognizer()
        
        //creamos la peticion
        let request = SFSpeechURLRecognitionRequest(url: audio)
        
        //Traducimos
        recognizer?.recognitionTask(with: request, resultHandler: { [unowned self](result, error) in
            //checamos que null no sea nula
            guard let result = result else{
                print("Ha habido un error: \(String(describing: error))")
                return
            }
            
            //Revisamos que haya terminado
            if result.isFinal {
                //sacamos el texto
                let text = result.bestTranscription.formattedString
                
                //Lo escribimos en un txt
                do{
                    try text.write(to: transcription, atomically: true, encoding: String.Encoding.utf8)
                    
                    //Lo agregamos al sportLight
                    self.indexMemory(memory: memory, text: text)
                }
                catch let error{
                    print(error.localizedDescription)
                }
            }
        })
    }
    
    //Configuramos los datos a agregar en el spotlight
    func indexMemory(memory: URL, text: String){
        let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeText as String)
        attributeSet.title = "Recuerdo de Glory Days"
        attributeSet.contentDescription = text
        attributeSet.thumbnailURL = thumbailURL(form: memory)
        
        let item = CSSearchableItem(uniqueIdentifier: memory.path, domainIdentifier: "com.practica.GloryDays", attributeSet: attributeSet)
        
        item.expirationDate = Date.distantFuture
        
        CSSearchableIndex.default().indexSearchableItems([item]) { (error) in
            if let error = error {
                print(error.localizedDescription)
            }
            else{
                print("se ha indexado correctamente \(text)")
            }
        }
    }
    
    //Para configurar el buscador para que aparesca
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        if section == 0 {
            return CGSize(width: 0, height: 50)
        }
        else{
            return CGSize.zero
        }
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let memory = self.filteredMemories[indexPath.row]
        
        let fileManager = FileManager.default
        
        do{
            let audioName = audioURL(form: memory)
            let transcriptionName = transcriptionURL(form: memory)
            
            
            if fileManager.fileExists(atPath: audioName.path) {
                //Sacamos el audio a reproducir
                self.audioPlayer = try AVAudioPlayer(contentsOf: audioName)
                
                //Reproducios el audio
                self.audioPlayer?.play()
            }
            
            if fileManager.fileExists(atPath: transcriptionName.path) {
                let contents = try String(contentsOf: transcriptionName)
                
                //Imprimimos el contenido en una alerta
                let mensaje : UIAlertController = UIAlertController(title: "It was a glorious day", message: contents, preferredStyle: .alert)
                let cerrar : UIAlertAction = UIAlertAction(title: "yes it was", style: .default)
                mensaje.addAction(cerrar)
                present(mensaje,animated: true)
            }
        }
        catch let error{
            print(error.localizedDescription)
        }
    }
    
    //MARK: - BUSCADOR DE LA APP
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterMemories(text: searchText)
    }
    
    //Ocultamos el teclado al momento que el usuairo termine de escribir
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func filterMemories(text: String){
        //Si el texto esta vacío devolvemos todas las memorias
        guard text.count > 0 else{
            self.filteredMemories = self.memories
            UIView.performWithoutAnimation {
                collectionView?.reloadSections(IndexSet(integer: 1))
            }
            return
        }
        
        var allTheItems : [CSSearchableItem] = []
        self.searchQuery?.cancel()
        
        //Hacemos la consulta como un like en SQL no importa si sea mayuscula o miniscula
        let queryString = "contentDescription == \"*\(text)*\"c"
        
        //Preparamos la consulta
        self.searchQuery = CSSearchQuery(queryString: queryString, attributes: nil)
        
        //Comenzamos a buscar de manera asincrona y todo lo que coincida con la busqueda lo guardamos
        self.searchQuery?.foundItemsHandler = { items in
            allTheItems.append(contentsOf: items)
        }
        
        //Lo que va a suceder al finalizar la consulta
        self.searchQuery?.completionHandler = { error in
            DispatchQueue.main.async { [unowned self] in
                self.activeteFilter(matches: allTheItems)
            }
        }
        
        //Ejecutamos
        self.searchQuery?.start()
    }
    
    //Pasamos de items a Memorias
    func activeteFilter(matches: [CSSearchableItem]){
        self.filteredMemories = matches.map { item in
            let uniqueID = item.uniqueIdentifier
            let url = URL(fileURLWithPath: uniqueID)
            return url
        }
        
        /*for item in filteredMemories{
            print(item)
            print("-------------------------")
            print("")
        }*/
        
        //print(filteredMemories.count)
        
        getCorrectFilterMemories()
        
        UIView.performWithoutAnimation {
            collectionView?.reloadSections(IndexSet(integer: 1))
        }
    }
    
    func getCorrectFilterMemories(){
        let fileManager = FileManager.default
        var correctURL : [URL] = []
        
        for i in 0..<filteredMemories.count{
            let url = filteredMemories[i]
            let thumbail = thumbailURL(form: url)
            if fileManager.fileExists(atPath: thumbail.path){
                //filteredMemories.remove(at: i)
                correctURL.append(filteredMemories[i])
            }
        }
        
        filteredMemories.removeAll()
        
        filteredMemories = correctURL
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
